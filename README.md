# README #

#### The image database generator is capable of adjusting to multiple collections such as: ####
* IMDB
* WIKI
* MORPH
* FERET
* FG-NET
* YFCC100m

####The software will generate a random balanced dataset based on the inputs that can be:####
* Gender
* Minimum Age
* Maximum Age

####After generating the database, it is possible to execute different cloud services such as:####
* Amazon Rekognition
* Microsoft Azure
* IBM Watson
* KAIROS

The Caffe pretrained model provided by Rasmus et al, can be found at https://data.vision.ee.ethz.ch/cvl/rrothe/imdb-wiki/ it is included as a local service that can be run freely.

### What is this repository for? ###

* Store and share open source software to researchers
* Version 1.0

### How do I get set up? ###

* Fork the application
* Run the scripts
* Install dependencies
* Make sure MongoDB is installed
* execute run.py for the database

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Screenshots ###

* Balanced dataset generator
    ![Scheme](images/database_generator.jpg)
    
* Plots
    ![Scheme](images/linear.jpg)
    Click on this link for the [Linear](https://plot.ly/~felix.andabasabe/54/underage-average-estimated-age-vs-real-age-for-female/) plot example
    ![Scheme](images/bar.jpg)
    Click on this link for the [Bar](https://plot.ly/~felix.andabasabe/58/underage-mae-per-age-per-service-for-male/) chart example
    ![Scheme](images/box.jpg)
    Click on this link for the [Box](https://plot.ly/~felix.andabasabe/64/underage-mae-by-service-srv-dex/) plot example

### Who do I talk to? ###

* Felix Anda
* [Forensics and security](https://forensicsandsecurity.com)