import bz2
import csv
import operator
import os
import random
from collections import OrderedDict

import itertools
from PIL import Image
from io import BytesIO
import json, xmljson

from bson import json_util
from lxml.etree import fromstring
from pymongo import MongoClient

from model.db_image import Database

cl = MongoClient()


class FERET(Database):
    def __init__(self):
        Database.__init__(self, "FERET")
        self.recordings = cl[self.get_name()]["recordings"]
        self.subjects = cl[self.get_name()]["subjects"]

    def get_recordings(self):
        return self.recordings

    def get_subjects(self):
        return self.subjects

    def set_image_path(self, record):
        image_path = os.path.join(self.get_root_path(), record['file_path'])
        self.image_path = image_path

    def insert_subject(self, file_path, disk):

        with open(file_path) as f:
            text = f.read()
            xml = fromstring(text)
            data = json_util.loads(json.dumps(xmljson.yahoo.data(xml)))

            if disk == 1:
                for a in data['Subjects']['Subject']:
                    self.get_subjects().insert(a)
            elif disk == 2:
                self.get_subjects().insert(data['Subjects']['Subject'])

    def insert_recordings(self, file_path, disk):

        with open(file_path) as f:
            text = f.read()
            xml_string = fromstring(text)
            convert = json.loads(json.dumps(xmljson.yahoo.data(xml_string)))

            if disk == 1:
                for a in convert['Recordings']['Recording']:
                    self.get_recordings().insert(a)
            elif disk == 2:
                self.get_recordings().insert(convert['Recordings']['Recording'])

    def get_age_all(self):

        for rec in self.get_recordings().find():
            capture_date = rec['CaptureDate']
            subject = self.get_subjects().find_one({'id': rec["Subject"]["id"]})
            yob = subject["YOB"]["value"]

            year_capture = capture_date[6:10]
            age = (int(year_capture) - int(yob))

            print(rec['id'], age, subject['id'], rec["URL"]["relative"])

    def get_gender(self, subject_id):

        documents = self.get_subjects().find_one({'id': subject_id})

        return documents['Gender']['value']

    def get_age_frontal_images(self):

        # filter multiple dudes by age

        dict_people_age = {}

        for rec in self.get_recordings().find({'URL.relative': {'$regex': u'_fa'}}):
            capture_date = rec['CaptureDate']
            subject = self.get_subjects.find_one({'id': rec["Subject"]["id"]})

            yob = subject["YOB"]["value"]
            year_capture = capture_date[6:10]
            age = (int(year_capture) - int(yob))
            subject_id = subject['id']
            gender = self.get_gender(self, subject_id)

            dict_people_age[rec['id']] = subject_id, age, gender

        d = {k: [i for x, i in v]
             for k, v in itertools.groupby(sorted((len(x), x) for x in dict_people_age.values()),
                                           key=operator.itemgetter(1))}

        od = (OrderedDict(sorted(d.items(), key=lambda t: t[0])))

        with open('feret_stats.csv', 'wb') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(['subject_id', 'age', 'gender'])
            for row in od:
                csv_out.writerow(row)

        print('-' * 80)

    def get_random_image(self, age_input, gender_input, filtered_images):

        gender_normalize = normalize_gender(gender_input)

        if gender_normalize is None:
            return gender_normalize

        images_dictionary = []

        processed_list = self.get_processed_list()

        #deleted_list = self.get_deleted_list()



        for rec in self.get_recordings().find(
                {
                    'URL.relative': {'$regex': u'_fa'},
                    'id': {"$nin": processed_list},
                    'URL.relative': {"$nin": processed_list},
                    #'_id': {"$nin": deleted_list}
                    '_id': {"$nin": filtered_images}
                }
        ):
            capture_date = rec['CaptureDate']
            subject = self.get_subjects().find_one({'id': rec["Subject"]["id"]})

            yob = subject["YOB"]["value"]
            year_capture = capture_date[6:10]
            age = (int(year_capture) - int(yob))
            subject_id = subject['id']
            gender = self.get_gender(subject_id)

            if age == age_input and gender == gender_normalize:
                relative = rec['URL']['relative']
                root_disk = rec['URL']['root']

                disk = 'dvd1/' if root_disk == 'Disc1' else 'dvd2/'

                file_path = disk + relative

                dict_people_age = {
                    '_id': rec['_id'],
                    'gender': gender,
                    'age': age,
                    'id': subject_id,
                    'file_path': file_path
                }

                images_dictionary.append(dict_people_age)

        if len(images_dictionary) > 0:

            random_image = random.choice(images_dictionary)

            id_random = random_image['file_path']

            self.get_random().insert(
                {'id': id_random}
            )

            return random_image

    def count_images(self, age_input, gender):

        # count only frontal images

        counter = 0

        gender_normalize = normalize_gender(gender)

        if gender_normalize is None:
            return gender_normalize

        processed_list = self.get_processed_list()

        deleted_list = self.get_deleted_list()

        for rec in self.get_recordings().find(
                {
                    'URL.relative': {'$regex': u'_fa'},
                    '_id': {"$nin": deleted_list},
                    'id': {"$nin": processed_list},
                    'URL.relative': {"$nin": processed_list}



                }
        ):
            capture_date = rec['CaptureDate']
            subject = self.get_subjects().find_one({'id': rec["Subject"]["id"]})
            yob = subject["YOB"]["value"]
            year_capture = capture_date[6:10]
            age = (int(year_capture) - int(yob))
            subject_id = subject['id']
            gender = self.get_gender(subject_id)

            if age == age_input and gender == gender_normalize:
                counter += 1

        return counter


def check_connection():
    from pymongo import MongoClient

    try:
        client = MongoClient()
        client.server_info()
        return client

    except:
        print('error')
        return None


def process_bz2_feret(input_file_path, output_folder ):
    zipfile = bz2.BZ2File(input_file_path)  # open the file
    data = zipfile.read()  # get the decompressed data

    file_jpg_data = BytesIO(data)
    im = Image.open(file_jpg_data)

    new_file = (os.path.basename(input_file_path)).replace('.ppm.bz2', '.jpg')

    im.save(os.path.join(output_folder, new_file))

    return new_file


def normalize_gender(gender):
    if gender == 0:
        return 'Female'
    elif gender == 1:
        return 'Male'
    else:
        print('gender problem')
        return None
