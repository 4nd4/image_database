import csv
import os
import random

from model.db_image import Database, get_na_list


class MEDS(Database):
    def __init__(self):
        Database.__init__(self, "MEDS")

    def set_image_path(self, record):
        file_name = record['img_name']
        folder = str(record['img_dname']).replace('./', '')
        image_path = os.path.join(self.get_root_path() + folder, file_name)
        self.image_path = image_path

    def upload_db_from_csv(self, file_path):
        with open(file_path) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.get_db().insert(row)

    def get_filtered_images(self, filtered_list):
        #list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        #list_na_service = get_na_list()

        #return list(set(list_deleted + list_processed + list_na_service))

        return list(set(list_processed + filtered_list))

    def get_record(self, age, gender, filtered_images):
        age_field = 'age_phd'
        gender_field = 'sex'
        id_field = '_id'

        # list_deleted = self.get_deleted_list()
        # list_processed = self.get_processed_list()
        # list_na_service = get_na_list()

        # filtered_list = list_deleted + list_processed + list_na_service

        gender_normalize = normalize_gender(gender)

        if gender_normalize is None:
            return gender_normalize

        image1 = self.get_db().find(
            {
                age_field: {"$regex": str(age) + '.'},
                gender_field: gender_normalize,
                #'_id': {"$nin": list_deleted},
                #id_field: {"$nin": list_processed}
                #id_field: {"$nin": filtered_list}
                id_field: {"$nin": filtered_images}
            }
        )

        image2 = self.get_db().find(
            {
                age_field: {"$regex": str(age - 1) + '.'},
                gender_field: gender_normalize,
                #'_id': {"$nin": list_deleted},
                #id_field: {"$nin": list_processed}
                # id_field: {"$nin": filtered_list}
                id_field: {"$nin": filtered_images}
            }
        )

        return {
            'image1': image1,
            'image2': image2
        }

    def count_images(self, age, gender, filtered_images):

        age_field = 'age_phd'

        counter = 0

        images = self.get_record(age, gender, filtered_images)

        id_list = []

        for a in images['image1']:
            if a['_id'] not in id_list:
                subject_id = a['_id']
                id_list.append(subject_id)
                age_value = round(float(a[age_field]))

                if age_value == age:
                    counter += 1

        for a in images['image2']:
            if a['_id'] not in id_list:
                subject_id = a['_id']
                id_list.append(subject_id)
                age_value = round(float(a[age_field]))

                if age_value == age - 1:
                    counter += 1

        return counter

    def get_random_image(self, age, gender, filtered_images):

        age_field = 'age_phd'

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images['image1']:
            if row not in images_dictionary:
                age_value = int(round(float(row[age_field])))
                row['id'] = row.pop('subject_id')

                if age_value == age:
                    row['age'] = age_value

                    images_dictionary.append(row)

        for row in images['image2']:
            if row not in images_dictionary:
                age_value = int(round(float(row[age_field])))

                row['id'] = row.pop('subject_id')

                if age_value == age:
                    row['age'] = age_value
                    images_dictionary.append(row)

        if len(images_dictionary) > 0:
            random_image = random.choice(images_dictionary)

            id_random = random_image['_id']

            self.get_random().insert(
                {'id': id_random}
            )

            return random_image

    def manage_paths(self, record):
        file_name = record['img_name']
        folder = str(record['img_dname']).replace('./', '')
        image_path = os.path.join(self.get_root_path() + folder, file_name)
        return image_path



def normalize_gender(gender):
    if gender == 0:
        return 'F'
    elif gender == 1:
        return 'M'
    else:
        print('Error normalizing gender')
        return None
