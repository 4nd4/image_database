from bson import Code
from pymongo import MongoClient

cl = MongoClient()


def get_service_list(service):

    """
gets list of images that already have been evaluated by the service
    :param service: prediction service
    :return: list of items
    """
    cl_service = cl[service.get_name()]["predictions"]

    list_service = []

    records = cl_service.find({}, {"subject_id": 1})

    for i in records:
        list_service.append(i['subject_id'])

    records.close()

    return list_service
