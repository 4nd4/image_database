def map_db_names(db_name):
    vowels = ('A', 'E', 'I', 'O', 'U')

    if len(db_name) == 4:
        return db_name

    if len(db_name) < 4:
        return db_name

    if len(db_name) > 4:

        for c in db_name:
            if c in vowels:
                db_name = db_name[:db_name.index(c)] + db_name[db_name.index(c) + 1:]

                if len(db_name) == 4:
                    return db_name


def check_max_file_set(min_value, max_value, db_list):

    # compute filtered images once

    dictionary_filter = {}

    database_dictionary = {}

    # get filtered images per database

    print('Retrieving filtered data...')

    for db in db_list:
        dictionary_filter[db] = db.get_filtered_images()

    for i in range(min_value, max_value):
        for binary_gender in (0, 1):

            for d in db_list:
                db_name = d.get_name()

                filtered_images = dictionary_filter[d]

                database_dictionary[db_name] = d.count_images(i, binary_gender, filtered_images)

            print(i, binary_gender, sum(database_dictionary.values()))


def is_number(s):
    try:
        float(s)  # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True


def normalize_get_gender(record):
    if 'gender' in record:

        gender_value = record['gender']

        if is_number(gender_value):
            if int(gender_value) == 0:
                return 'Female'
            elif int(gender_value) == 1:
                return 'Male'
            else:
                print('gender error')
        else:
            if gender_value in ('M', 'F'):
                return 'Female' if gender_value == 'F' else 'Male'
            elif gender_value in ('Male', 'Female'):
                return gender_value

    elif 'sex' in record:

        gender_value = record['sex']

        if gender_value in ('M', 'F'):
            return 'Female' if gender_value == 'F' else 'Male'


def normalize_get_age(record):
    if record is not None and 'age' in record:
        return int(record['age'])
    elif record is not None and 'age_phd' in record:
        return int(round(float(record['age_phd'])))
    else:
        print(record)
