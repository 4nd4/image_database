import os
import shutil


def delete_folder(path_directory_creation):
    for the_file in os.listdir(path_directory_creation):
        file_path = os.path.join(path_directory_creation, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)


def rename_file(record, dst_dir, old_file_name, service):
    dst_file = os.path.join(dst_dir, old_file_name)

    extension = os.path.splitext(old_file_name)[1]

    new_file_name = str(record['_id']) + extension
    new_dst_file_name = os.path.join(dst_dir, new_file_name)

    if os.path.isfile(new_dst_file_name):
        print('duplicate file found in', service.get_name())
        os.unlink(dst_file)
        return None
    else:
        os.rename(dst_file, new_dst_file_name)

    return new_dst_file_name


def iterate_folder(path_directory_creation, gender=None):

    list_folder = []

    for root, subFolders, files in os.walk(path_directory_creation, topdown=True):
        for f in files:
            if f.lower().endswith(".jpg") and folder_gender(root, gender):
                record_id = os.path.splitext(os.path.basename(f))[0]
                list_folder.append(record_id)

    return list_folder


def folder_gender(path, gender):
    if gender is None:
        return True

    return str(gender).lower() == os.path.basename(os.path.normpath(path)).lower()


def delete_file_physically(file_path):
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
            print('file_deleted')
        else:
            print('file not found', file_path)
    except Exception as e:
        print(e)


def check_integer_list(directory_list):
    for l in directory_list:
        if not l.isdigit():
            return False

    return True


def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def count_images_folder(directory, required_images):

    list_images_required = []

    for root, dirs, files in os.walk(directory):
        if os.path.basename(root) in ['Female', 'Male']:
            age = os.path.basename(os.path.dirname(root))
            gender = os.path.basename(root)

            if len(files) != required_images:

                dict_images = {
                    'age': age,
                    'gender': gender,
                    'number': int(required_images - len(files))
                }

                list_images_required.append(dict_images)

    return list_images_required
