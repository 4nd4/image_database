import os
import random

from model.db_image import Database, get_na_list


class MORPH(Database):
    def __init__(self):
        Database.__init__(self, "MORPH")

    def set_image_path(self, record):
        file_name = record['id'] + '.JPG'
        image_path = os.path.join(self.get_root_path(), file_name)
        self.image_path = image_path

    def upload_database(self, root_dir):

        for root, subFolders, files in os.walk(root_dir, topdown=True):
            for f in files:
                if f.lower().endswith(".jpg"):
                    id_file = os.path.splitext(f)[0]
                    id_subject = get_code(f)
                    gender = get_gender(f)
                    age = get_age(f)

                    self.get_db().insert_one({
                        'id': id_file,
                        'id_subject': id_subject,
                        'gender': gender,
                        'age': age
                    })

    def get_filtered_images(self, filtered_list):
        #list_deleted = self.get_deleted_list()
        #list_na_service = get_na_list()

        #return list(set(list_deleted + list_na_service))

        return list(set(filtered_list))

    def get_record(self, age, gender, filtered_images):
        gender_normalize = normalize_gender(gender)

        if gender_normalize is None:
            return gender_normalize

        #list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        #list_na_service = get_na_list()

        #filtered_list = list_deleted + list_na_service

        images = self.get_db().find(
            {
                'age': str(age),
                'gender': gender_normalize,
                #'_id': {"$nin": list_deleted},
                #'_id': {"$nin": filtered_list},
                '_id': {"$nin": filtered_images},
                'id_subject': {"$nin": list_processed}
            }
        )

        return images

    def count_images(self, age, gender, filtered_images):
        images = self.get_record(age, gender, filtered_images)
        return images.count()

    def get_random_image(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        images_dictionary = []

        for row in images:
            images_dictionary.append(row)

        if len(images_dictionary) > 0:
            random_image = random.choice(images_dictionary)

            id_random = random_image['_id']

            self.get_random().insert(
                {'id': id_random}
            )

            return random_image

    def manage_paths(self, record):
        file_name = record['id'] + '.JPG'
        image_path = os.path.join(self.get_root_path(), file_name)
        return image_path


def get_code(file):
    code = file[:6]
    return code


def get_gender(file):
    gender = file[-7:-6]
    return gender


def get_age(file):
    age = file[-6:-4]
    return age


def normalize_gender(gender):
    if gender == 0:
        return 'F'
    elif gender == 1:
        return 'M'
    else:
        print('Error normalizing gender')
        return None
