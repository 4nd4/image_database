import os
import csv

from model.db_image import Database, get_na_list


class FGNET(Database):
    def __init__(self):
        Database.__init__(self, "FGNET")

    def set_image_path(self, record):
        file_name = record['id'] + '.JPG'
        image_path = os.path.join(self.get_root_path(), file_name)
        self.image_path = image_path

    def upload_database(self, root_dir):

        for root, subFolders, files in os.walk(root_dir, topdown=True):
            for f in files:
                if f.lower().endswith(".jpg"):
                    id_file = os.path.splitext(f)[0]
                    id_subject = get_code(f)
                    gender = get_gender(f)
                    age = get_age(f)

                    self.get_db().insert_one({
                        'id': id_file,
                        'id_subject': id_subject,
                        'gender': gender,
                        'age': age
                    })

    def count_images(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        return images.count()

    def get_random_image(self, age, gender, filtered_images):

        images = self.get_record(age, gender, filtered_images)

        from random import randint

        count_number = 0 if images.count() == 0 else images.count() - 1

        if count_number > 0:
            random_number = randint(0, count_number)

            # add record to random

            id_random = images[random_number]['id']

            self.get_random().insert(
                {'id': id_random}
            )

            return images[random_number]

    def get_filtered_images(self, filtered_list):

        #list_deleted = self.get_deleted_list()
        #list_na_service = get_na_list()

        #return list(set(list_deleted + list_na_service))

        return list(set(filtered_list))

    def get_record(self, age, gender, filtered_images):

        gender_normalize = normalize_gender(gender)

        if gender_normalize is None:
            return gender_normalize

        #list_deleted = self.get_deleted_list()
        list_processed = self.get_processed_list()
        #list_na_service = get_na_list()

        #filtered_list = list_deleted + list_na_service

        images = self.get_db().find(
            {
                'age': age,
                'gender': gender_normalize,
                'id': {"$nin": list_processed},
                #'_id': {"$nin": list_deleted}
                #'_id': {"$nin": filtered_list}
                '_id': {"$nin": filtered_images}
            }
        )

        return images

    def manage_paths(self, record):
        file_name = record['id'] + '.JPG'
        image_path = os.path.join(self.get_root_path(), file_name)
        return image_path



def get_code(file):
    code = file[:3]
    return code


def get_age(file):
    code = file[4:6]
    return int(code)


def get_gender(file):
    file_path = "D:/Documents/research/fg-net\FGNETAgeEstimation/fgnet_age_estimations.csv"

    with open(file_path) as csvfile:
        doc = csv.reader(csvfile, delimiter=',')

        for row in doc:
            code = row[1][6:9]
            file_code = get_code(file)

            if file_code == code:
                gender = row[1][10:11]
                return gender
    return None


def count_files_gender(root_dir):
    male_dict = {}
    female_dict = {}

    for root, subFolders, files in os.walk(root_dir, topdown=True):
        for f in files:
            if f.lower().endswith(".jpg"):

                gender = get_gender(f)

                age = get_age(f)

                if gender == 'M':
                    if age in male_dict:
                        male_dict[age] += 1
                    else:
                        male_dict[age] = 1

                if gender == 'F':
                    if age in female_dict:
                        female_dict[age] += 1
                    else:
                        female_dict[age] = 1

    for key in sorted(female_dict):
        print("%s,%s" % (key, female_dict[key]))


def count_files(root_dir):

    data = {}

    for root, subFolders, files in os.walk(root_dir, topdown=True):
        for f in files:
            if f.lower().endswith(".jpg"):

                id_file = os.path.splitext(f)[0]
                id_subject = get_code(f)
                gender = get_gender(f)
                age = get_age(f)

                data[id_file] = id_subject, age, gender

    with open('fgnet_stats.csv', 'wb') as out:
        csv_out = csv.writer(out)
        csv_out.writerow(['id_subject', 'age', 'gender'])
        for row in data.values():
            csv_out.writerow(row)


def normalize_gender(gender):
    if gender == 0:
        return 'F'
    elif gender == 1:
        return 'M'
    else:
        print('Error normalizing gender')
        return None