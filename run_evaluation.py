import configuration
from controller import cn_evaluation, cn_image


path = configuration.path_directory_creation

images = cn_image.Query(configuration.db_list, path)

if False:
    # executes predictions only on the directory specified
    cn_evaluation.execute_prediction(configuration.kairos_service, path)
    print('done')

if False:
    cn_evaluation.print_stats(path, configuration.db_list, configuration.list_services)

if False:

    if cn_evaluation.run_service(configuration.ibm_service, configuration.db_list):
        print('OK')
    else:
        print('ENDING')

if False:
    # careful... if I exceed 5000 it charges me

    # run kairos service over databases selected

    if cn_evaluation.run_service(configuration.microsoft_service, configuration.db_list):
        print('OK')
    else:
        print('ENDING')

if False:
    #cn_evaluation.execute_prediction(configuration.microsoft_service, path)
    #cn_evaluation.execute_prediction(configuration.ibm_service, path)
    cn_evaluation.run_service(configuration.amazon_service, configuration.db_list)

if True:
    cn_evaluation.run_service(configuration.dex_service, configuration.db_list)

if False:
    configuration.amazon_service.get_real_age_and_predicted(configuration.db_list, 21)  # Highest age of majority (Bahrain)
