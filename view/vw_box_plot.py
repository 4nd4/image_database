import plotly.plotly as py
import plotly.graph_objs as go
import configuration
from controller import cn_evaluation

py.plotly.tools.set_credentials_file(
    username= configuration.plotly_credentials['username'],
    api_key=configuration.plotly_credentials['api_key']
)

path = configuration.path_directory_creation


def plot_graph(min_value, max_value, title_text, service, gender=None):

    x_axis = [i for i in range(min_value, max_value + 1)]

    data = []


    if gender is None:

        graph_data_male = cn_evaluation.get_box_plot(path, configuration.db_list, service, 'male')
        graph_data_female = cn_evaluation.get_box_plot(path, configuration.db_list, service, 'female')

        for keys, values in graph_data_male.iteritems():
            trace_male = go.Box(
                y=values,
                name=keys,
                marker=dict(
                    color='#3D9970',
                    opacity=0
                ),
                #line=dict(width=5),

                #whiskerwidth=0.5
            )

            data.append(trace_male)

        for keys, values in graph_data_female.iteritems():
            trace_female = go.Box(
                y=values,
                name=keys,
                marker=dict(
                    color='#FF4136'
                ),
                #whiskerwidth=1
            )

            data.append(trace_female)

    else:
        graph_data = cn_evaluation.get_box_plot(path, configuration.db_list, service, gender)

        for keys, values in graph_data.iteritems():
            trace = go.Box(
                y=values,
                name=keys,
                marker=dict(
                    color='#3D9970',
                    opacity=0
                ),
                # line=dict(width=5),

                # whiskerwidth=0.5
            )

            data.append(trace)

    trace0 = go.Scatter(
        x=x_axis,
        y=x_axis,
        name='100%',
        showlegend=False,
        line=dict(
            color='rgb(0, 0, 0)',
            width=1,
            dash='dot'
        )
    )

    data.append(trace0)

    if gender is not None:
        format_gender = 'for ' + gender
    else:
        format_gender = ''

    layout = go.Layout(

        title=title_text + 'MAE by Service ({0}{1})'.format(service.get_name(), format_gender),

        yaxis=dict(
            title='predicted ages',
            zeroline=False
        ),

        boxmode='group',
        boxgap=0,
        boxgroupgap=0,
        showlegend=False

    )

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename= title_text + '_box_graph')


plot_graph(13, 23, 'Underage-',configuration.microsoft_service, 'male')