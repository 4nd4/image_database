import plotly.plotly as py
import plotly.graph_objs as go
import configuration
from controller import cn_evaluation

# TODO: how to include male and females

py.plotly.tools.set_credentials_file(
    username= configuration.plotly_credentials['username'],
    api_key=configuration.plotly_credentials['api_key']
)

path_directory_creation = 'D:/Documents/research/evaluation/dataset_paper/'

path = path_directory_creation

attributes = [
        {
            "id": 1,
            "colors": {'R': 44, 'G': 52, 'B': 68},        # Kairos (GREY)
            "symbol": "diamond-tall"
        },
        {
            "id": 2,
            "colors": {'R': 255, 'G': 153, 'B': 0},      # Amazon (YELLOW)
            "symbol": "triangle-up"
        },
        {
            "id": 3,
            "colors": {'R': 255, 'G': 0, 'B': 0},        # DEX
            "symbol": "cross"
        },
        {
            "id": 4,
            "colors": {'R': 124, 'G': 187, 'B': 0},       # Microsoft (2nd green colour)
            "symbol": "square"
        },
        {
            "id": 5,
            "colors": {'R': 70, 'G': 107, 'B': 176},     # IBM (BLUE)
            "symbol": "circle"
        }
    ]


def plot_graph():

    data = []

    counter = 0
    service_counter = 0

    #service = configuration.dex_service

    for service in configuration.list_services:

        R = attributes[service_counter]["colors"]['R']
        G = attributes[service_counter]["colors"]['G']
        B = attributes[service_counter]["colors"]['B']

        colorString = 'rgb({0},{1},{2})'.format(R, G, B)

        graph_data = cn_evaluation.get_plot_data_mae_intervals (path, configuration.db_list, service)

        '''
        graph_data = {
            0:[1,2,1,1.5],
            1:[2,2,1,1.5],
            2:[1,1,1,1.5]
        }
        '''

        service_counter +=1

        ranges = {
            0: '[0-9]',
            1: '[10-19]',
            2: '[20-29]',
            3: '[30-39]',
            4: '[40-49]',
            5: '[50-59]',
            6: '[60-69]',
            7: '[70-77]',
        }

        for keys, values in graph_data.iteritems():
            trace = go.Box(
                y=values,
                name=ranges[keys],
                marker=dict(
                    color=colorString,

                ),
            )

            data.append(trace)

    layout = go.Layout(
        title='MAE per age range per service',
        yaxis=dict(
            #title='MAE per ranges per service',
            zeroline=False,
        ),
        boxmode='group',
        boxgap=0,
        boxgroupgap=0,
        showlegend=False


    )

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename='box_graph-test')    #change this according to need


plot_graph()