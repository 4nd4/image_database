import plotly.plotly as py
import plotly.graph_objs as go
import configuration
from controller import cn_evaluation

py.plotly.tools.set_credentials_file(
    username= configuration.plotly_credentials['username'],
    api_key=configuration.plotly_credentials['api_key']
)

path = configuration.path_directory_creation


def plot_graph(min_value, max_value, title_text, gender=None):

    x_axis = [i for i in range(min_value, max_value+1)]

    data = []

    attributes = [
        {
            "id": 1,
            "colors": {'R': 44, 'G': 52, 'B': 68},        # Kairos (GREY)
            "symbol": "diamond-tall"
        },
        {
            "id": 2,
            "colors": {'R': 255, 'G': 153, 'B': 0},      # Amazon (YELLOW)
            "symbol": "triangle-up"
        },
        {
            "id": 3,
            "colors": {'R': 255, 'G': 0, 'B': 0},        # DEX
            "symbol": "cross"
        },
        {
            "id": 4,
            "colors": {'R': 124, 'G': 187, 'B': 0},       # Microsoft (2nd green colour)
            "symbol": "square"
        },
        {
            "id": 5,
            "colors": {'R': 70, 'G': 107, 'B': 176},     # IBM (BLUE)
            "symbol": "circle"
        }
    ]

    counter = 0

    for service in configuration.list_services:

        R = attributes[counter]["colors"]['R']
        G = attributes[counter]["colors"]['G']
        B = attributes[counter]["colors"]['B']

        # get plot data for mae

        y_axis_service = cn_evaluation.get_plot_data_mae(path, configuration.db_list, service, gender).values()

        colorString = 'rgb({0},{1},{2})'.format(R, G, B)

#        mae = cn_evaluation.mae(y_axis_service,x_axis)

        legend_service = service.get_name()

        legend_name = normalize_legend(legend_service)

        trace_service = go.Bar(
            x=x_axis,
            y=y_axis_service,
            name=legend_name,
            marker=dict
            (
                color=colorString
            )



        )
        data.append(trace_service)
        counter += 1

    if gender is not None:
        format_gender = 'for ' + gender
    else:
        gender = ''
        format_gender = ''

    # Edit the layout
    layout = dict(
        barmode='group',
        title=title_text + 'MAE per age per service {0}'.format(format_gender),
        xaxis=dict(
            title='Age',
            showline=False
        ),
        yaxis=dict(
            title='MAE',
            showline=False
        ),
        autosize=True,
        font=dict(size=20)

    )

    filename = 'bar-plot' + '-' + title_text + gender

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=filename)


def normalize_legend(legend_name):
    if legend_name == 'SRV_AMAZON':
        return 'AWS'
    elif legend_name == 'SRV_MICROSOFT':
        return 'Azure'
    elif legend_name == 'SRV_KAIROS':
        return 'Kairos'
    elif legend_name == 'SRV_DEX':
        return 'DEX'


plot_graph(0, 30, '[0-30]','female')
