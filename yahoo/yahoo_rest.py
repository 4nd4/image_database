import requests
from pymongo import MongoClient

import configuration

cl = MongoClient()
cl_data = cl["YAHOO"]["data"]


def search(tags, page):

    method = 'flickr.photos.search'
    api_key = configuration.flickr_api_key
    api_secret = configuration.flickr_api_secret
    endpoint = 'https://api.flickr.com/services/rest/'
    tag_mode = 'any'    #and or any

    resp = requests.get(
        endpoint +
        "?method=" + method +
        '&api_key=' + api_key +
        '&api_secret=' + api_secret +
        '&tag_mode=' + tag_mode +
        '&tags=' + tags +
        '&format=json' +
        #'&per_page=500' #+
        '&page=' + str(page) +
        '&nojsoncallback=1'
    )

    if resp.status_code != 200:
        print('Error', resp.status_code)
        return False
    else:

        record = resp.json()

        number_pages = record['photos']['pages']

        if page > number_pages:
            print('page is greater than available')
            return False
        elif page == number_pages:
            return True
        else:
            cl_data.insert(record)
            search(tags, page+1)


search(tags='birthday,1,baby', page=1)

print('ele')
