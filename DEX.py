import os

import configuration
from model.db_service import Service

os.environ['GLOG_minloglevel'] = '2'
from model import db_image, db_functions

import matplotlib.pyplot as plt
import caffe

from skimage import io; io.use_plugin('matplotlib')

project_root = 'D:/Download/data-vision/'

class Dex(Service):
    def __init__(self):
        Service.__init__(self, "SRV_DEX", -1)

    def set_response(self, image_path):

        plt.rcParams['figure.figsize'] = (4, 4)
        plt.rcParams['image.interpolation'] = 'nearest'
        plt.rcParams['image.cmap'] = 'gray'

        age_net_pretrained = project_root + 'models/dex/dex_chalearn_iccv2015.caffemodel'
        age_net_model_file = project_root + 'models/dex/age.prototxt'
        gender_net_pretrained = project_root + 'models/dex/gender.caffemodel'
        gender_net_model_file = project_root + 'models/dex/gender.prototxt'

        mean_filename = project_root + 'imagenet_mean.binaryproto'


        try:

            with open(mean_filename, "rb") as proto_data:
                a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data.read())

            mean = caffe.io.blobproto_to_array(a)[0]

            age_net = caffe.Classifier(age_net_model_file, age_net_pretrained,
                                       mean=mean,
                                       channel_swap=(2, 1, 0),
                                       raw_scale=255,
                                       image_dims=(256, 256))

            gender_net = caffe.Classifier(gender_net_model_file, gender_net_pretrained,
                                          mean=mean,
                                          channel_swap=(2, 1, 0),
                                          raw_scale=255,
                                          image_dims=(256, 256))

            input_image = caffe.io.load_image(image_path)
            age_prediction = age_net.predict([input_image])
            gender_prediction = gender_net.predict([input_image])

            predicted_age = age_prediction[0].argmax()
            predicted_gender = "Female" if gender_prediction[0].argmax() == 0 else "Male"

            self.response = {
                'age': int(predicted_age),
                'gender': predicted_gender
            }

            # clear mem

            del input_image
            del age_net
            del gender_prediction

        except Exception as e:
            print(e)
            print('error')

    def get_real_age_and_predicted(self, db_config, gender=None):

        list_dictionary = []

        # get only saved elements

        records = self.get_records_stats(gender)

        for r in records:

            subject_id = r['subject_id']

            data = db_image.get_data(db_config.get_db_list(), subject_id)

            real_age = db_functions.normalize_get_age(data)

            if 'age' in r:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': r['age']
                }

                list_dictionary.append(row)

            else:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': 'N/A'
                }

                if configuration.enable_csv_write_NA:
                    list_dictionary.append(row)

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated

    def process_na_service(self):
        return []