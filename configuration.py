import DEX
import amazon
import how_old
import ibm
import kairos
import azure
from feret.feret import FERET
from fgnet.fgnet import FGNET
from flickr.flickr import FLICKR
from imdb.imdb import IMDB
from meds.meds import MEDS
from morph.morph import MORPH
from wiki.wiki import WIKI

# value to configure excel capability. Must be turned off for graphs
enable_csv_write_NA = False

# [azure api_key]
azure_api_key = '[ENTER_KEY_HERE]'

# [flickr-configuration]
flickr_api_key = u'[ENTER_KEY_HERE]'
flickr_api_secret = u'[ENTER_KEY_HERE]'

# [plotly-configuration]
plotly_credentials = {
    'username': '[ENTER_USERNAME]',
    'api_key': '[ENTER_KEY_HERE]'
}

# [alert-configuration]
mail_username = '[ENTER_EMAIL_HERE]'
smtp_server = '[ENTER_SMTP_SERVER_HERE]'
smtp_port = 587
alert_username = '[ENTER_EMAIL_HERE]'
alert_namespace = 'alert'

# [path-configuration]
fgnet_root_path = 'D:/Documents/research/fg-net/Images/'
feret_root_path = 'D:/Documents/research/feret/colorferet/'

imdb_crop_root_path = 'D:/Download/data-vision/imdb_crop/'
imdb_root_path = 'D:/Download/data-vision/imdb/'

wiki_root_path = 'D:/Download/data-vision/wiki.tar/wiki/'
wiki_crop_root_path = 'D:/Download/data-vision/wiki_crop/'

morph_root_path = 'D:/Documents/research/morph/Images_ori/'
meds_root_path = 'D:/Documents/research/meds/NIST_SD32_MEDS-II_face/'

yahoo_dataset_path = 'D:/yahoo/yfcc100m_dataset/yfcc100m_dataset'

path_directory_creation = 'D:/Documents/research/evaluation/dataset/'

temp_directory = 'D:/temp/'

flickr_root_path = 'D:/Documents/research/evaluation/flickr_manual_images/'

fgnet_db = FGNET()
feret_db = FERET()
wiki_db = WIKI()
imdb_db = IMDB()
morph_db = MORPH()
meds_db = MEDS()
flickr_db = FLICKR()

wiki_db.set_root_path(wiki_root_path)
imdb_db.set_root_path(imdb_root_path)
morph_db.set_root_path(morph_root_path)
feret_db.set_root_path(feret_root_path)
fgnet_db.set_root_path(fgnet_root_path)
meds_db.set_root_path(meds_root_path)
flickr_db.set_root_path(flickr_root_path)


db_list = [
    fgnet_db,
    wiki_db,
    imdb_db,
    morph_db,
    meds_db,
    flickr_db,
    #feret_db, fsanda always off

    ]

kairos_service = kairos.Kairos()
amazon_service = amazon.Amazon()
dex_service = DEX.Dex()
microsoft_service = azure.Azure()
ibm_service = ibm.IBM()
how_old_service = how_old.HowOld()

list_services = [
    kairos_service,
    amazon_service,
    dex_service,
    microsoft_service,
    #ibm_service
    how_old_service
]