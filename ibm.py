from watson_developer_cloud import VisualRecognitionV3 as vr
from model import db_image, db_functions
from model.db_service import Service

#instance =vr(api_key='c917afa979a13bbc62224da7f911a843f2856c1d', version='2017-01-06')


class IBM(Service):
    def __init__(self):
        Service.__init__(self, "SRV_IBM", 250)

        #self.subscription_key = '2c1b186b84df20c2e0d19e4521125302bbdd2ef6'  #sa.4
        #self.subscription_key = '062796413965187bd4051f9e62e2f330094fb5b5'  #daniell
        self.subscription_key = '15493c9cc49d9c933006065b20f3f1c82a411d51'
        #self.subscription_key = 'c442e456add1e9907bc8e4c49ebebfb68ad9b0e2' #dbacud

        self.version = '2017-08-09'
        self.exception = None
        self.instance = vr(api_key=self.subscription_key, version=self.version)

    def set_exception(self, exception):
        self.exception = exception

    def get_exception(self):
        return self.exception

    def get_subscription_key(self):
        return self.subscription_key

    def get_version(self):
        return self.version

    def get_instance(self):
        return self.instance

    def set_response(self, image_path):
        self.response = self._process_response(image_path)

    def _process_response(self, image_path):
        with open(image_path, "rb") as img_file:
            img = self.get_instance().detect_faces(images_file=img_file)
            return img

    def get_real_age_and_predicted(self, images):

        list_dictionary = []

        records = self.get_records_stats()

        for r in records:

            subject_id = r['subject_id']

            #data = cn_image.get_data(subject_id)
            data = db_image.get_data(images.get_db_list(), subject_id)

            real_age = db_functions.normalize_get_age(data)

            #predicted_age = 'N/A'

            if 'images' in r:

                img = r['images']

                for i in img:
                    if 'faces' in i:
                        faces = i['faces']

                    for f in faces:
                        if 'age' in f:
                            age = f['age']

                            if 'min' in age and 'max' not in age:
                                predicted_age = age['min']
                            elif 'max' in age and 'min' not in age:
                                predicted_age = age['max']
                            elif 'min' in age and 'max' in age:
                                predicted_age = (int(age['min']) + int(age['max']))/2
                                #predicted_age = age['min']

                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': predicted_age
                }

                list_dictionary.append(row)

            else:
                row = {
                    'subject_id': subject_id,
                    'real_age': real_age,
                    'predicted_age': "N/A"
                }

                # list_dictionary.append(row) #TODO: this has to be controlled figure what I should do when values are N/A here and in Kairos

        decorated = sorted(list_dictionary, key=lambda k: k['real_age'])

        return decorated
